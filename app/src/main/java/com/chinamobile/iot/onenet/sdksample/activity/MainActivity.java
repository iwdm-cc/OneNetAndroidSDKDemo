package com.chinamobile.iot.onenet.sdksample.activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.chinamobile.iot.onenet.OneNetApi;
import com.chinamobile.iot.onenet.sdksample.R;
import com.chinamobile.iot.onenet.sdksample.utils.Preferences;

public class MainActivity extends AppCompatActivity {

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);

        String apikey = OneNetApi.getAppKey();
        if (TextUtils.isEmpty(apikey) || apikey.contains(" ")) {
            apikey = Preferences.getInstance(this).getString(Preferences.API_KEY, null);
        }
        if (TextUtils.isEmpty(apikey) || apikey.contains(" ")) {
            //跳转编辑界面
            OneNetApi.setAppKey("");
            startActivity(new Intent(this, EditApiKeyActivity.class));
        } else {
            //设置 apikey
            OneNetApi.setAppKey(apikey);
        }
        // 跳转到设备管理界面
        DeviceActivity.actionDevice(this, null);
    }
}
