package com.chinamobile.iot.onenet.sdksample.activity;

import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.MenuItem;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.NotificationCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.chinamobile.iot.onenet.OneNetApi;
import com.chinamobile.iot.onenet.OneNetApiCallback;
import com.chinamobile.iot.onenet.sdksample.R;
import com.chinamobile.iot.onenet.sdksample.model.DSItem;
import com.chinamobile.iot.onenet.sdksample.model.DeviceItem;
import com.chinamobile.iot.onenet.sdksample.utils.DeviceItemDeserializer;
import com.chinamobile.iot.onenet.sdksample.utils.IntentActions;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DeviceActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {

    public static final String EXTRA_DEVICE_ITEM = "extra_device_item";
    private Toolbar mToolbar;
    private RecyclerView mRecyclerView;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private DSListAdapter mAdapter;
    private DeviceItem mDeviceItem;
    private BroadcastReceiver mUpdateDeviceReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            OneNetApi.querySingleDevice(mDeviceItem.getId(), new OneNetApiCallback() {
                @Override
                public void onSuccess(String response) {
                    JsonObject resp = new JsonParser().parse(response).getAsJsonObject();
                    int errno = resp.get("errno").getAsInt();
                    if (0 == errno) {
                        GsonBuilder gsonBuilder = new GsonBuilder();
                        gsonBuilder.registerTypeAdapter(DeviceItem.class, new DeviceItemDeserializer());
                        Gson gson = gsonBuilder.create();
                        mDeviceItem = gson.fromJson(resp.get("data"), DeviceItem.class);
                        getSupportActionBar().setTitle(mDeviceItem.getTitle());
                    } else {
                        String error = resp.get("error").getAsString();
                        Toast.makeText(getApplicationContext(), error, Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailed(Exception e) {
                    e.printStackTrace();
                }
            });
        }
    };
    private BroadcastReceiver mUpdateDsList = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            getDataStreams();
        }
    };

    public static void actionDevice(Context context, DeviceItem deviceItem) {
        Intent intent = new Intent(context, DeviceActivity.class);
        intent.putExtra(EXTRA_DEVICE_ITEM, deviceItem);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_device);
        LocalBroadcastManager.getInstance(this).registerReceiver(mUpdateDeviceReceiver, new IntentFilter(IntentActions.ACTION_UPDATE_DEVICE));
        LocalBroadcastManager.getInstance(this).registerReceiver(mUpdateDsList, new IntentFilter(IntentActions.ACTION_UPDATE_DS_LIST));

        getDevices();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mUpdateDeviceReceiver);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mUpdateDsList);
    }

    private void getDevices() {
        int mCurrentPage = 1;
        Map<String, String> urlParams = new HashMap<>();
        urlParams.put("page", String.valueOf(mCurrentPage));
        urlParams.put("per_page", "10");
        OneNetApi.fuzzyQueryDevices(urlParams, new OneNetApiCallback() {
            @Override
            public void onSuccess(String response) {
                JsonObject resp = new JsonParser().parse(response).getAsJsonObject();
                int errno = resp.get("errno").getAsInt();
                if (0 == errno) {
                    parseData(resp.get("data").getAsJsonObject());
                } else {
                    String error = resp.get("error").getAsString();
                    Toast.makeText(DeviceActivity.this, error, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailed(Exception e) {
                e.printStackTrace();
            }
        });
    }

    private void parseData(JsonObject data) {
        System.out.println(data + "11");
        if (null == data) {
            return;
        }
        System.out.println("data:" + data);
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(DeviceItem.class, new DeviceItemDeserializer());
        Gson gson = gsonBuilder.create();
        JsonArray jsonArray = data.get("devices").getAsJsonArray();
        List<DeviceItem> devices = new ArrayList<>();
        for (JsonElement element : jsonArray) {
            devices.add(gson.fromJson(element, DeviceItem.class));
        }
        initViews(devices.get(0));
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            startActivity(new Intent(this, EditApiKeyActivity.class));
        }
        return super.onOptionsItemSelected(item);
    }

    private void initViews(DeviceItem mDeviceItem) {
        this.mDeviceItem = mDeviceItem;
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle(mDeviceItem.getTitle());
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.abc_ic_ab_back_material);

        mRecyclerView = (RecyclerView) findViewById(R.id.recyler_view);
        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout);

        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mAdapter = new DSListAdapter();
        mRecyclerView.setAdapter(mAdapter);
        mSwipeRefreshLayout.setColorSchemeColors(0xFFDA4336);
        mSwipeRefreshLayout.setOnRefreshListener(this);
        getDataStreams();
        mSwipeRefreshLayout.setRefreshing(true);

    }

    @Override
    public void onRefresh() {
        getDataStreams();
    }


    private void getDataStreams() {
        OneNetApi.queryMultiDataStreams(mDeviceItem.getId(), new OneNetApiCallback() {
            @Override
            public void onSuccess(String response) {
                mSwipeRefreshLayout.setRefreshing(false);
                JsonObject resp = new JsonParser().parse(response).getAsJsonObject();
                int errno = resp.get("errno").getAsInt();
                if (0 == errno) {
                    JsonElement dataElement = resp.get("data");
                    if (dataElement != null) {
                        JsonArray jsonArray = dataElement.getAsJsonArray();
                        System.out.println(jsonArray + "jsonArray");
                        ArrayList<DSItem> dsItems = new ArrayList<>();
                        Gson gson = new Gson();
                        double adcx = 0;
                        boolean fire = false;
                        for (JsonElement element : jsonArray) {
                            DSItem ds = gson.fromJson(element, DSItem.class);
                            if ("adcx".equals(ds.getId())) {
                                adcx = (double) ds.getCurrentValue();
                            }

                            if ("fire".equals(ds.getId())) {
                                fire = (double) ds.getCurrentValue() >= 1;

                            }
                            dsItems.add(ds);
                        }

                        if (fire && adcx > 2300) {
                            System.out.println("警报");
                            ClickBt1();
                        }
                        mAdapter.setNewData(dsItems);
                    }
                } else {
                    String error = resp.get("error").getAsString();
                    Toast.makeText(getApplicationContext(), error, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailed(Exception e) {
                mSwipeRefreshLayout.setRefreshing(false);
                e.printStackTrace();
            }
        });
    }

    public void ClickBt1() {
        NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this)
                .setContentTitle("请注意")
                .setContentText("厨房检测较大可能发生火灾 ，请尽快处理")
                .setWhen(System.currentTimeMillis())
                .setSmallIcon(R.drawable.img_2)
                .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.img_2));

        SharedPreferences yyx = getSharedPreferences("yyx", MODE_PRIVATE);
        int notify = yyx.getInt("notify", 0);
        //通过 builder.build() 拿到 notification
        mNotificationManager.notify(notify++, mBuilder.build());
        yyx.edit().putInt("notify",notify).apply();

    }

    class DSListAdapter extends BaseQuickAdapter<DSItem, BaseViewHolder> {
        public DSListAdapter() {
            super(R.layout.ds_list_item);
        }

        @Override
        protected void convert(BaseViewHolder helper, DSItem item) {
            helper.setText(R.id.id, getName(item.getId()));
            if (item.getCurrentValue() != null) {
//                helper.setText(R.id.current_value, getResources().getString(R.string.latest_data) + ": " + String.format("%.1f", Double.parseDouble(item.getCurrentValue().toString())) + item.getUnitSymbol());
                helper.setText(R.id.current_value, getResources().getString(R.string.latest_data) + ": " + String.format("%.1f", Double.parseDouble(item.getCurrentValue().toString())) + "°C");
            } else {
                helper.setText(R.id.current_value, getResources().getString(R.string.latest_data) + ": " + getResources().getString(R.string.no_data));
            }
            if (!TextUtils.isEmpty(item.getUpdateTime())) {
                helper.setText(R.id.update_time, getResources().getString(R.string.update_time) + ": " + item.getUpdateTime());
            } else {
                helper.setText(R.id.update_time, getResources().getString(R.string.update_time) + ": " + getResources().getString(R.string.no_data));
            }
            helper.setImageResource(R.id.img, getImg(item.getId()));
        }

        private int getImg(String id) {
            switch (id) {
                case "temp":
                    return R.drawable.img_3;
                case "humidity":
                    return R.drawable.shidu;
                case "light":
                    return R.drawable.sun;
                case "adcx":
                    return R.drawable.img_1;
                case "fire":
                    return R.drawable.img_4;
            }
            return R.drawable.ic_add;
        }

        private String getName(String id) {
            switch (id) {
                case "temp":
                    return "温度";
                case "humidity":
                    return "湿度";
                case "light":
                    return "光照";
                case "adcx":
                    return "烟雾浓度";
                case "fire":
                    return "火焰情况";
            }
            return id;
        }

    }
}
