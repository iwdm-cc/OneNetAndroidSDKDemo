package com.chinamobile.iot.onenet.sdksample.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.chinamobile.iot.onenet.OneNetApi;
import com.chinamobile.iot.onenet.sdksample.R;
import com.chinamobile.iot.onenet.sdksample.utils.Preferences;
import com.google.android.material.textfield.TextInputLayout;

import static com.chinamobile.iot.onenet.sdksample.utils.IntentActions.ACTION_UPDATE_APIKEY;


public class EditApiKeyActivity extends AppCompatActivity {

    private Toolbar mToolbar;
    private TextInputLayout mTextInputLayout;
    private Preferences mPreferences;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_api_key);

        mPreferences = Preferences.getInstance(this);

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);

        mTextInputLayout = (TextInputLayout) findViewById(R.id.text_input_layout);
        String apikey = mPreferences.getString(Preferences.API_KEY, "");
        if (0 == apikey.length()) {
            apikey = OneNetApi.getAppKey();
        }
        mTextInputLayout.getEditText().setText(apikey);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;

            case R.id.menu_done:
                String apiKey = mTextInputLayout.getEditText().getText().toString();
                OneNetApi.setAppKey(apiKey.trim());
                mPreferences.putString(Preferences.API_KEY, apiKey);
                LocalBroadcastManager.getInstance(this).sendBroadcast(new Intent(ACTION_UPDATE_APIKEY));
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_done, menu);
        return super.onCreateOptionsMenu(menu);
    }
}
